package me.khabib.squatscounter;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private long lastUpdate = 0;
    private float lastX = 0, lastY = 0, lastZ = 0;
    private final double NOISE = 0.01;
    private boolean calibration=false;
    private int cCal=0;

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        mSensorManager.registerListener(this, mSensor, 1);
    }

    public void onSensorChanged(SensorEvent event) {
        TextView sensorFiled = (TextView) findViewById(R.id.sensor);
        TextView xFiled = (TextView) findViewById(R.id.x);
        TextView yField = (TextView) findViewById(R.id.y);
        TextView zFiled = (TextView) findViewById(R.id.z);
        assert sensorFiled != null;
        Sensor currSensor = event.sensor;
        if (currSensor.getType() == Sensor.TYPE_GRAVITY) {
            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 500) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];


                float deltaX = x-lastX;
                float deltaY = y-lastY ;
                float deltaZ = z-lastZ ;
                if (Math.abs(deltaX) < NOISE) deltaX = (float) 0.0;
                if (Math.abs(deltaY) < NOISE) deltaY = (float) 0.0;
                if (Math.abs(deltaZ) < NOISE) deltaZ = (float) 0.0;
                if (calibration){

                }
                lastX = x;
                lastY = y;
                lastZ = z;
                if (Math.sqrt(deltaX * deltaX / 2) > 1)
                    sensorFiled.setText("The device is moved horizontally.");
                if (Math.sqrt(deltaY * deltaY / 2) > 1)
                    sensorFiled.setText("The device is moved vertically.");

                assert xFiled != null;
                xFiled.setText(Float.toString(round(x, 2)));
                assert yField != null;
                yField.setText(Float.toString(round(y, 2)));
                assert zFiled != null;
                zFiled.setText(Float.toString(round(z, 2)));
            }

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "Старт приложения", Toast.LENGTH_SHORT).show();
    }
}
